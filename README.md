# soal-shift-sisop-modul-1-E01-2022

Laporan soal dan penyelesaian untuk modul 1 mata kuliah sistem operasi kelas E tahun 2022


## DAFTAR ISI ##
- [Daftar Anggota Kelompok](#daftar-anggota-kelompok)
- [Nomor 1](#nomor-1)
    - [Soal  1.a](#soal-1a)
    - [Soal  1.b](#soal-1b)
    - [Soal  1.c](#soal-1c)
    - [Soal  1.d](#soal-1d)
- [Nomor 2](#nomor-2)
    - [Soal  2.a](#soal-2a)
    - [Soal  2.b](#soal-2b)
    - [Soal  2.c](#soal-2c)
    - [Soal  2.d](#soal-2d)
    - [Soal  2.e](#soal-2e)
- [Nomor 3](#nomor-3)
    - [Soal  3.a](#soal-3a)
    - [Soal  3.b](#soal-3b)
    - [Soal  3.c](#soal-3c)
    - [Soal  3.d](#soal-3d)
- [Dokumentasi](#dokumentasi)
## Daftar Anggota Kelompok ##

NRP | NAMA | KELAS
--- | --- | ---
5025201237  | Putu Ravindra Wiguna | SISOP E
5025201003    | Rahmat Faris Akbar | SISOP E
05111740000146    | Muhammad Kiantaqwa Farhan | SISOP E

## Nomor 1 ##
Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun,
karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan
senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk
mempermudah pekerjaan mereka, Han membuat sebuah program.

### Soal 1.a ###
Han membuat sistem register pada script register.sh dan setiap user yang berhasil
didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login
yang dibuat di script main.sh

#### Dokumentasi Source Code ####
```bash
function prepare_dir {
    mkdir -p "./user/"

    touch "./user/user.txt"
}
```

#### Cara Pengerjaan ####
1. Membuat script `register.sh` 
2. Pada script `register.sh` dibuat folder `./user/` sebagai parent dengan syntax `mkdir -p` (fungsi dari `-p` adalah agar saat membuat folder dengan nama yang sama dengan nama folder parent, maka tidak akan ada pesan error, akan dibuat sub-folder dengan nama tersebut)
3. Membuat sebuah file bernama `user.txt` pada folder yang barusan dibuat menggunakan syntax `touch`
4. Membuat script `main.sh`

#### Kendala ####
Tidak ada

### Soal 1.b ###
Demi menjaga keamanan, input password pada login dan register harus
tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut
i. Minimal 8 karakter
ii. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
iii. Alphanumeric
iv. Tidak boleh sama dengan username
#### Dokumentasi Source Code ####
```bash
function get_user_name {
    echo "Username: "
    read uname
}
```
```bash
function validate_pass {
    local hasMoreThanEight=0
    if [[ ${#passwd} -ge 8 ]]
    then
        hasMoreThanEight=1
    else
        echo "Password must be at least 8 characters long"
    fi

    local hasAtleastOneUpper=0
    #per oper oper
    local totalUpper=$(echo "$passwd" | grep -o '[A-Z]' | wc -l)
    if [ $totalUpper -ge 1 ]
    then
        hasAtleastOneUpper=1
    else
        echo "Password must have at least one uppercase letter"
    fi

    local hasAtleastOneLower=0
    local totalLower=$(echo "$passwd" | grep -o '[a-z]' | wc -l)
    if [ $totalLower -ge 1 ]
    then
        hasAtleastOneLower=1
    else
        echo "Password must have at least one lowercase letter"
    fi

    #alpha numeric test
    local isAlphaNumeric=1
    if [[ "$passwd" =~ [^a-zA-Z0-9] ]]
    then
        echo "Password must only contains alphanumeric character"
        isAlphaNumeric=0
    fi

    #check kesamaan with username
    local isDifferentWithUser=1
    if [[ "$passwd" == "$uname" ]]
    then
        isDifferentWithUser=0
        echo "Password must be different then username"
    fi

    isValidPass=0
    #combine it all, >=8, atleast 1upper,1lower, alpha numerik, diff user
    if [[ $hasMoreThanEight == 1 && $hasAtleastOneLower == 1 && $hasAtleastOneUpper == 1 && $isAlphaNumeric == 1 && $isDifferentWithUser == 1 ]]
    then
        isValidPass=1
    fi
}
```
```bash
function get_pass {
    local isDone=1
    while [ $isDone -eq 1 ]
    do
        echo "Enter password for ["$uname"]: "
        read -s passwd
        echo "Confirm the password (type it again):"
        read -s confirm_pass

        validate_pass
        if [[ $isValidPass == 1 ]]
        then
            if [ $passwd == $confirm_pass ]
            then
                echo "Password confirmed!"
                isDone=$(($isDone + 1))
            else
                echo "Password didn't match"
            fi
        else
            echo "Password is not valid"
        fi
    done
}
```
```bash
function create_account {
    #check if username already exist or not
    if [[ -z ${saved_username_map["$uname"]} ]]
    then
        #means null, so not exist, so acc
        echo "$uname" >> "user/user.txt" && echo "Successfully create account"   #add to last line to user, and give sign it is succed
        echo "$passwd" >> "user/user.txt" && echo "Successfully added password"   #add to last line to user, and give sign it is succed
    else
        #oh no already exist
        echo "Username already exist"
        message_code=1
        add_log
    fi
}
```
```bash
function get_saved_user {
    #loop through .user/user.txt
    local counter=1
    while read p
    do
        if [ $counter  ==  1 ]
        then
            # echo "$p"
            #insert it to the map
            saved_username_map["$p"]=1
        else
            #counter is > 1, mostlikely 2
            counter=0 #reset it
        fi
        counter=`expr $counter + 1`
    done < "./user/user.txt"
}
```
```bash
function get_pass {
    echo "Password: "
    read -s passwd
}
```

#### Cara Pengerjaan ####
Pada `register.sh`
1. Pada fungsi `get_user_name` user akan diminta untuk membuat username, dan program akan membaca username tersebut sebagai variabel `uname`
2. Pada fungsi `validate_pass` akan dilakukan pengecekan beberapa syarat password yang bisa dibuat sesuai dengan perintah pada soal
3. Pertama akan dicek apakah password sudah terdiri dari minimal 8 karakter atau belum menggunakan conditional statements dan syntax `-ge`. Jika sudah memenuhi syarat maka variabel `hasMoreThanEight`(sebagai boolean) yang awalnya diset bernilai 0 akan diset menjadi 1. Sebaliknya, akan dikeluarkan output string `Password must be at least 8 characters long`.
4. Lalu, akan dicek apakah password sudah memiliki minimal 1 huruf besar atau belum dengan cara menghitung terlebih dahulu banyak huruf besar pada password. Caranya adalah menggunakan `echo` untuk menampilkan `passwd`. Kemudian hasilnya akan dioper menggunakan syntax `|` dan difilter untuk mendapatkan huruf besarnya menggunakan syntax `grep -o [A-Z]`. Lalu, dioper lagi untuk dihitung jumlahnya dengan syntax `wc -l`. Selanjutnya, dengan conditional statements dan syntax `-ge`. Jika sudah ada 1 atau lebih huruf besar, maka variabel `hasAtleastOneUpper` (sebagai boolean) yang awalnya diset bernilai 0 akan diset menjadi 1. Sebaliknya, akan dikeluarkan output string `Password must have at least one uppercase letter`.
5. Langkah yang hampir sama dengan langkah keempat diulangi lagi untuk mengecek apakah password sudah memiliki minimal 1 huruf kecil atau belum. Letak perbadaannya hanya pada pemfilterannya mencari yang huruf kecil `[a-z]`.
6. Kemudian akan dicek apakah password sudah memenuhi syarat alphanumeric. Pada langkah ini terdapat variabel `isAlphaNumeric` yang berfungsi seperti boolean dan diset nilai awalnya adalah 1. Kemudian, dengan conditional statements akan dicek dengan syarat `=~ [^a-zA-Z0-9]` (terdiri dari selain huruf alfabet besar maupun kecil dan digit 0 sampai 9) yang artinya jika syarat tersebut terpenuhi, maka akan dicetak output string `Password must only contains alphanumeric character` dan nilai variabel `isAlphaNumeric` akan diset menjadi 0. 
7. Lalu akan dicek apakah password dan username sama atau tidak. Digunakan conditional statements seperti biasa. Jika ternyata password dan usernama sama, maka variabel `isDifferentWithUser` (berfungsi seperti boolean) yang asalnya bernilai 1, akan diubah nilainya menjadi 0, dan akan dikeluarkan output string `Password must be different then username`. Jika tidak, maka tidak dilakukan apa-apa (nilai variabel `isDifferentWithUser` tetap 1 yang berarti antara password dan username berbeda).
8. Terakhir, semua syarat di atas akan digabung menjadi satu. Digunakan conditional statements, apabila semua syarat di atas terpenuhi maka variabel `isValidPass` yang aslinya bernilai 0 akan diset nilainya menjadi 1. Jika tidak, maka tidak dilakukan apa-apa (nilai variabel `isValidPass` tetap 0 yang berarti password yang dibuat tidak dapat diterima).
9. Pada fungsi `get_pass` akan dilakukan loop untuk membaca password yang diinputkan oleh user. Pada proses penginputan password ini, digunakan syntax `-s` agar password yang diinputkan tidak terlihat/hidden sehingga lebih aman.

Pada `main.sh`
1. Pada fungsi `get_pass` propram akan meminta inputan password dari user.
2. Program membaca inputan variabel `passwd` dari user dan untuk menyembunyikan inputan (tidak menampilkan inputan di terminal) digunakan syntax `-s`

#### Kendala ####
Tidak ada

### Soal 1.c ###
Setiap percobaan login dan register akan tercatat pada log.txt dengan format :
MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi
yang dilakukan user.
i. Ketika mencoba register dengan username yang sudah terdaftar, maka
message pada log adalah REGISTER: ERROR User already exists
ii. Ketika percobaan register berhasil, maka message pada log adalah REGISTER:
INFO User USERNAME registered successfully
iii. Ketika user mencoba login namun passwordnya salah, maka message pada
log adalah LOGIN: ERROR Failed login attempt on user USERNAME
iv. Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User
USERNAME logged in

#### Dokumentasi Source Code ####
```bash
function create_account {
    #check if username already exist or not
    if [[ -z ${saved_username_map["$uname"]} ]]
    then
        #means null, so not exist, so acc
        echo "$uname" >> "user/user.txt" && echo "Successfully create account"   #add to last line to user, and give sign it is succed
        echo "$passwd" >> "user/user.txt" && echo "Successfully added password"   #add to last line to user, and give sign it is succed
    else
        #oh no already exist
        echo "Username already exist"
        message_code=1
        add_log
    fi
}
```
```bash
function add_log {
    touch "log.txt"

    local date_now=`date +"%D %T"`
    # echo "$date_now"
    local message_str=""
    if [ $message_code == 1 ]
    then
        message_str="REGISTER: ERROR User already exists"
    elif [ $message_code == 2 ]
    then
        message_str="REGISTER: INFO User $uname registered successfully"
    else
        message_str="Should not happen, something error"
    fi

    echo "$date_now $message_str" >> "log.txt"

}
```
```bash
function main {
    message_code=0
    prepare_dir
    #create empty array to hold saved username
    declare -A saved_username_map
    get_saved_user
    
    clear
    show_intro_msg
    
    echo "======================"
    echo "    CREATE ACCOUNT    "
    echo "======================"

    echo "Please enter this data to complete registration"
    get_user_name
    get_pass

    echo "Create this account?"
    echo "username: "$uname
    get_confirmation
    if [ $isAccepted -eq 1 ]
    then
        echo "Creating account ..."
        create_account
        if [ $message_code != 1 ]
        then
            message_code=2
            add_log
        fi
    else
        echo "Abort creating account"
    fi
}
```

```bash
function log_in {
    #check if username is exist
    if [[ -z ${user_pass_map["$uname"]} ]]
    then
        #password to that username is nothing
        echo "Username doesn't exist"
    else
        #exist the password
        if [ "$passwd" == "${user_pass_map["$uname"]}" ]
        then
            #wow success
            echo "====================="
            echo "   Login successful  "
            echo "====================="
            isLogedIn=1
            isDoneLoggingIn=1
            message_code=2
            add_log
        else
            #password did not match
            echo "Failed log in"
            message_code=1
            add_log
        fi
    fi
}

```
```bash
function add_log {
    touch "log.txt"

    local date_now=`date +"%D %T"`
    # echo "$date_now"
    local message_str=""
    if [ $message_code == 1 ]
    then
        message_str="LOGIN: ERROR Failed login attempt on user $uname"
    elif [ $message_code == 2 ]
    then
        message_str="LOGIN: INFO User $uname logged in"
    else
        message_str="Should not happen, something error"
    fi

    echo "$date_now $message_str" >> "log.txt"

}
```
#### Cara Pengerjaan ####
Pada Fungsi `add_log` di `register.sh`
1. Digunakan syntax `touch` untuk membuat file `log.txt` yang akan digunakan untuk menyimpan setiap percobaan login dan register
2. Digunakan variabel lokal `date_now` untuk menyimpan waktu sesuai dengan format `MM/DD/YY hh:mm:ss`
3. Kemudian digunakan conditional statements. Jika nilai dari variabel `message code` yang sebelumnya sudah diassign pada fungsi `create_account` adalah 1 maka artinya proses register tidak berhasil, sehingga variabel `message_str` akan memiliki isi seperti yang diminta soal, yaitu `REGISTER: ERROR User already exists`. Namun, jika nilai dari variabel `message code` adalah 2 (dari proses conditional statements fungsi `main`), maka artinya proses login berhasil, sehingga variabel `message_str` akan memiliki isi seperti yang diminta soal, yaitu `REGISTER: INFO User $uname registered successfully`. Selain itu, variabel `message_str` akan memiliki isi `Should not happen, something error`.
4. Nilai dari variabel `data_now` dan `message_str` akan dikirimkan dan ditambahkan di akhir file `log.txt`

Pada Fungsi `add_log` di `main.sh`
1. Digunakan syntax `touch` untuk membuat file `log.txt` yang akan digunakan untuk menyimpan setiap percobaan login dan register
2. Digunakan variabel lokal `date_now` untuk menyimpan waktu sesuai dengan format `MM/DD/YY hh:mm:ss`
3. Kemudian digunakan conditional statements. Jika nilai dari variabel `message code` yang sebelumnya sudah diassign pada fungsi `log_in` adalah 1 maka artinya proses login tidak berhasil, sehingga variabel `message_str` akan memiliki isi seperti yang diminta soal, yaitu `LOGIN: ERROR Failed login attempt on user $uname`. Namun, jika nilai dari variabel `message code` adalah 2, maka artinya proses login berhasil, sehingga variabel `message_str` akan memiliki isi seperti yang diminta soal, yaitu `LOGIN: INFO User $uname logged in`. Selain itu, variabel `message_str` akan memiliki isi `Should not happen, something error`.
4. Nilai dari variabel `data_now` dan `message_str` akan dikirimkan dan ditambahkan di akhir file `log.txt`

#### Kendala ####
Tidak ada

### Soal 1.d ###
Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai
berikut :
i. dl N ( N = Jumlah gambar yang akan didownload)
Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan
jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan
dimasukkan ke dalam folder dengan format nama
YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki
format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01,
PIC_02, dst. ). Setelah berhasil didownload semua, folder akan otomatis di
zip dengan format nama yang sama dengan folder dan dipassword sesuai
dengan password user tersebut. Apabila sudah terdapat file zip dengan
nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu,
barulah mulai ditambahkan gambar yang baru, kemudian folder di zip
kembali dengan password sesuai dengan user.
ii. att 
Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari
user yang sedang login saat ini.

#### Dokumentasi Source Code ####
```bash
function get_user_cmd {
    read cmd optional
    # echo $cmd "--" $optional 
    if [ "$cmd" == "dl" ]
    then
        download_picture $optional
    elif [ "$cmd" == "att" ]
    then
        show_login_att
    elif [ "$cmd" == "exit" ]
    then
        echo "Exiting Program..."
        isExit=1
    else
        echo "command not recognized"
    fi
}
```
```bash
function download_picture {
    #passed first argument as total image
    local total_image=$1
    
    local date_now=`date +%F`
    local dir_name="${date_now}_$uname"
    local cur_total_zipped=0
    #check if zip file exist
    FILE_ZIP="$dir_name.zip"
    if [ -f "$FILE_ZIP" ]
    then
        #exist, so unzip
        echo "Already exist a zip file with the same name, unziping it"
        unzip -P "$passwd" "$FILE_ZIP"
        #get current file total file
        cur_total_zipped=`zipinfo -h $FILE_ZIP | awk '/number/ {print $9}'`
        cur_total_zipped=$(($cur_total_zipped - 1))
        echo "found: $cur_total_zipped file inside folder in the zip"
         echo "Deleting old zip file"
        rm "$FILE_ZIP"
        echo "Finished"

    fi
    #download the picture
    for ((i=1;i<=$total_image;i++))
    do  
        echo "downloading image: [$i/$total_image]"
        wget "https://loremflickr.com/320/240" -P "$dir_name"
    done

    #change the name, using awk to get the name
    #save current internal field separator (because we will change it to new line)
    TEMP_IFS=$IFS
    list_img=`ls "$dir_name" -1 -c`
    #change ifs to new line
    IFS=$'\n'
    #convert to array sir
    list_img=($list_img)
    arr_len=${#list_img[@]}
    #preserve past ifs
    IFS=$TEMP_IFS

    #get max power of 10 as prefix
    #by dividing len of array by ten, and ceil it (a.k.a +10 then divide)
    local temp=$(($arr_len + 10))
    local prefix=""
    local power_of_ten=0
    #not in func because need 2 global var
    while [ $temp -gt 0 ]
    do
        power_of_ten=$(($power_of_ten + 1))
        temp=$(($temp / 10))
        prefix+="0"
    done
    # echo "prefix: $prefix"
    local len_dirname="${#dir_name}"
    #loop through the array and change the name
    for ((i=0;i<$arr_len;i++))
    do
        echo "change name [$(($i + 1))/$arr_len]"
        local cur_filename="$dir_name/${list_img[i]}"
        # echo "$cur_filename"
        # echo "${cur_filename:0:4}"
        # echo "${cur_filename:0:$((${#dir_name} + 5))}"
        #check if the name is like PIC
        if [ "${cur_filename:0:$(($len_dirname + 5))}" == "$dir_name/PIC_" ]
        then
            #already in PIC mode, so skip
            # echo "Already in PIC Mode skipping"
            #fix formatin
            local pic_num="${cur_filename:$(($len_dirname + 5)):$((${#cur_filename} - $len_dirname - 5))}"
            # echo "$pic_num"
            #if like curlen if pic num is less than uh len of prefix, then we change them too sir
            if [ ${#pic_num} -lt ${#prefix} ]
            then
                #change the name
                mv "$cur_filename" "$dir_name/PIC_${prefix:0:$((${#prefix} - ${#pic_num}))}$pic_num"
            fi
            continue
        fi
        #process prefix number
        get_power_of_ten $(($cur_total_zipped + 1))
        # if [ $compare_p_10 ] use substring sir
        name_prefix="${prefix:0:$(($power_of_ten - $compare_p_10))}"
        mv "$cur_filename" "$dir_name/PIC_${name_prefix}$(($cur_total_zipped + 1))"
        cur_total_zipped=$(($cur_total_zipped + 1))
    done

    #zip it all reccursively with passowrd
    zip -rP $passwd "$FILE_ZIP" "$dir_name"
    #remove the folder
    rm -rf "$dir_name"
}
```
```bash
function show_login_att {
    #use awk
    echo "Total login attempts for this user $uname:"
    #get log with login and current user
    cat "log.txt" | awk -v user=$uname '
    {if( ($0 ~ user) && ($0 ~ "LOGIN") ) {
        print  $0
    }}' | wc -l
    
}
```
#### Cara Pengerjaan ####
Perintah dl N (download)
1. Program membaca inputan dari fungsi `get_user_cmd` 
2. Setelah mendapat perintah/command `dl N` dari user, maka akan diambil total image dari argumen yang dipassing ke `download_picture`
3. Membuat folder dengan format sesuai pada soal (YYYY-MM-DD_USERNAME)
4. Perhatikan bahwa terdapat kasus dimana apabila sudah pernah didownload dengan user yang sama, maka harus diunzip terlebih dahulu. Sehingga diperlukan pengecekan terlebih dahulu menggunakan conditional statements `if`. Setelah diunzip.
5. Dilakukan proses download dengan loop dan wget
6. Mengganti nama, dihitung power of 10 untuk mengetahui panjang digit dari nama gambar yang akan digunakan sebagai prefix (misalkan terdapat 1000 gambar, maka nama gambar akan dimulai dari PIC_0001).
7. Setelah selesai melakukan rename, maka akan dilakukan zip dan foldernya akan dihapus

Perintah att (jumlah percobaan login)
1. Digunakan syntax `cat` untuk menampilkan isi dari file `log.txt`
2. Digunakan syntax `|` untuk mengoper hasil dari perintah sebelumnya ke `awk`
3. File tersebut difilter untuk diambil baris yang memiliki user name saat ini
4. Hasil dari `awk` tersebut dioper menggunakan syntax `|` kemudian ihitung jumlah barisnya menggunakan syntax `wc -l`

#### Kendala ####
Pada solusi untuk perintah dl N, sebenarnya bisa dibuat agar menjadi lebih mudah. Tidak perlu mengganti nama.Tetapi, pada waktu pengerjaan soal shift modul ini kami belum tahu cara set nama untuk wget. Sehingga terpaksa dilakukan cara manual (didownload terlebih dahulu, kemudian baru diganti namanya).

## Nomor 2 ##
Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak
bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan.Dapos langsung membuka log website dan menemukan banyak request yang berbahaya.
Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk
bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:

### Soal 2.a ###
Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.
#### Dokumentasi Source Code ####
```bash
FORENSIC_LOGDIR="forensic_log_website_daffainfo_log"
mkdir -p "$FORENSIC_LOGDIR"
```
#### Cara Pengerjaan ####
1. Membuat variabel `FORENSIC_LOGDIR` untuk mempersingkat nama folder saja
2. Membuat folder sebagai parent dengan syntax `mkdir -p` (fungsi dari `-p` adalah agar saat membuat folder dengan nama yang sama dengan nama folder parent, maka tidak akan ada pesan error, akan dibuat sub-folder dengan nama tersebut)

#### Kendala ####
Tidak ada

### Soal 2.b ###
Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak,
Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke
website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama
ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.
#### Dokumentasi Source Code ####
```bash
function calculate_seconds {
    #$1, $2, $3 is h, m, s
    echo "$(($1 * 3600 + $2 * 60 + $3))"
}
```
```bash
function calc_float {
    echo "$1" | bc -l
}
```
```bash
function solve_2b {
    #get rata-rata request per jam, baru masukin hasilnya ke ratarata.txt di folder atas
    #use awk sir
    #save the time on req_times
    local req_times=$(cat "log_website_daffainfo.log" | awk '
    BEGIN {FS=":"}
    {printf "%s:%s:%s\n", $3, $4, substr($5, 1, 2)}
    ')
    #convert req_times to array    
    TEMP_IFS=$IFS
    #change ifs to new line
    IFS=$'\n'
    #convert to array sir
    req_times=($req_times)
    local arr_len=${#req_times[@]}
    #preserve past ifs
    IFS=$TEMP_IFS

    #the table name
    # echo "${req_times[0]}"
    #start of first time
    local start_time="${req_times[1]}"
    local h_start=${start_time:0:2}
    local m_start=${start_time:3:2}
    local s_start=${start_time:6:2}
    # echo "$h_start:$m_start:$s_start"

    # echo "total second $(calculate_seconds $h_start $m_start $s_start)"
    local seconds_start=$(calculate_seconds $h_start $m_start $s_start)
    # echo "${req_times[1]}"
    #last time
    local end_time="${req_times[$arr_len-1]}"
    local h_end=${end_time:0:2}
    local m_end=${end_time:3:2}
    local s_end=${end_time:6:2}
    # echo "$h_end:$m_end:$s_end"
    # echo "total second $(calculate_seconds $h_end $m_end $s_end)"
    local seconds_end=$(calculate_seconds $h_end $m_end $s_end)
    # echo "${req_times[$arr_len-1]}"
    # echo "$arr_len"

    local total_seconds=$(($seconds_end - $seconds_start))
    # echo "$total_seconds"
    local req_per_sec=$(calc_float "$(($arr_len - 1))/$total_seconds")
    local req_per_hour=$(calc_float "$req_per_sec * 3600")

    # echo $req_per_hour
    # printf "1. Average request per hour is %.2f req/h\n" $req_per_hour
    # printf "Average request per hour is %.2f req/h\n" $req_per_hour > "$FORENSIC_LOGDIR/ratarata.txt"
    printf "Rata-rata serangan adalah sebanyak %.2f requests per jam" $req_per_hour > "$FORENSIC_LOGDIR/ratarata.txt"
}
```
#### Cara Pengerjaan ####
1. Ambil data waktu pada `log_website_daffainfo.log` menggunakan awk dengan field separator `:`
2. Ambil string ke 3,4, dan 5(hanya 2 karakter pertama, tanpa tanda petik)
3. Simpan data waktu tersebut ke variabel lokal `req_times`
4. Digunakan IFS (Internal Field Separator) untuk mengubah ke new line terlebih dahulu. Kemudian akan diubah ke bentuk array
5. Ambil `start_time` dan `end_time` yang digunakan sebagai bahan untuk menghitung selisih waktunya menggunakan fungsi `calculate_seconds`
6. Cara kerja dari fungsi `calculate_seconds` adalah dengan menerima 3 argumen berupa jam (h), menit (m), dan juga detik (s) dari `start_time` maupun `end_time`. Sehingga `start_time` akan dipisah menjadi `h_start`, `m_start`, dan `s_start`. Begitu juga dengan `end_time`. Setelah menerima 3 argumen, fungsi `calculate_seconds` akan melakukan konversi dengan mengubah jam ke detik (dikali 3600), menit ke detik (dikali 60), kemudian menjumlahkan jam, menit, & detik yang sudah dikonversi ke detik. Hasilnya akan di `echo` dan disimpan.
6. Hitung selisih dari `seconds_start` (start_time dalam satuan detik) dan `seconds_end` (end_time dalam satuan detik). Kemudian assign hasilnya ke variabel `total_seconds`.
7. Hitung rata-rata request per detik dengan cara membagi banyak request (banyak baris dikurangi 1 karena baris pertama tidak dihitung) dengan total waktu dalam detik
8. Hitung rata-rata request per jam dengan cara mengkalikan rata-rata request per second dengan 3600 (karena dalam 1 jam sama dengan 3600 detik)
9. Dikarenakan perhitungan di bash itu menggunakan integer, maka digunakan funsi bantuan yaitu `calc_float` agar angka hasil perhitungan dari langkah ke 7 dan 8 lebih akurat
10. Kemudian hasil nilai rata-rata request per jam nya akan dicetak dan dikirim ke folder yang sudah dibuat sebelumnya, disimpan sebagai file `ratarata.txt`.

#### Kendala ####
Nilai rata-rata request per jam dari program (82.36) yang sudah dibuat hasilnya masih berbeda dengan kunci jawaban saat demo. (81.0...).

### Soal 2.c ###
Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke
website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak
melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan
dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt
kedalam folder yang sudah dibuat sebelumnya.
#### Dokumentasi Source Code ####
```bash
function solve_2c {
    local ip_list=$(cat "log_website_daffainfo.log" | awk '
    BEGIN {FS=":"}
    {printf "%s\n", $1}
    ')

    #convert it to array    
    TEMP_IFS=$IFS
    #change ifs to new line
    IFS=$'\n'
    #convert to array sir
    ip_list=($ip_list)
    local arr_len=${#ip_list[@]}
    #preserve past ifs
    IFS=$TEMP_IFS
    # echo "$arr_len"
    declare -A ip_map
    #mappuh
    local modusIP=""
    local curMax=-1
    for ((i=0;i<$arr_len;i++))
    do
        # echo "${ip_list[$i]}"
        #check if ip map of this ip is null
        local curIP=${ip_list[$i]}
        if [ -z ${ip_map["$curIP"]} ]
        then
            #tidak ada, so null
            ip_map["$curIP"]=1
            # echo "${ip_list[$i]} is 1"
        else
            #ada sir, increment
            # echo "mantap"
            ip_map["$curIP"]=$((${ip_map["$curIP"]} + 1))
            if [ ${ip_map["$curIP"]} -gt $curMax ]
            then
                # echo "${ip_list[$i]} is ${ip_map["${ip_list[$i]}"]}"
                #update curmax and modusIP
                curMax=${ip_map["$curIP"]}
                modusIP=${ip_list[$i]}
            fi
        fi
    done
    # echo "2. IP with most request is: $modusIP, having $curMax requests" 
    # echo "IP with most request is: $modusIP, having $curMax requests" > "$FORENSIC_LOGDIR/result.txt"
    echo "IP yang paling banyak mengakses server adalah: ${modusIP:1:${#modusIP}-2} sebanyak $curMax" > "$FORENSIC_LOGDIR/result.txt"

}
```
#### Cara Pengerjaan ####
1. Membuat variabel `ip_list` yang berisikan data seluruh IP yang melakukan penyerangan terhadap website. Cara mendapatkan seluruh IP penyerang adalah dengan menggunakan `awk`. Pertama, file `log_website_daffainfo.log` akan dicetak menggunakan syntax `cat`. Kemudian hasilnya difilter dengan field operator `:` dan diambil yang berada pada posisi 1 saja (posisi dari IP).
2. Kemudian data IP yang berupa string tadi akan diubah menjadi array (langkahnya sama dengan soal 2.c)
3. Dilakukan looping untuk menghitung berapa kali setiap IP melakukan penyerangan terhadap website. Caranya adalah dengan menyimpan IP ke `ip_map`. Jika IP belum ada di `ip_map`, maka IP tersebut akan dihitung 1. Jika IP sudah ada di `ip_map`, maka akan dilakukan increment terhadap banyaknya IP tersebut.
4. Dalam looping tersebut juga terdapat variabel `modusIP` untuk menyimpan alamat IP yang paling banyak menyerang website. Banyaknya penyerangan terbanyak oleh suatu IP disimpan pada variabel `curMax`. 
5. Hasil dari looping tersebut akan dicetak sesuai dengan format kalimat yang sudah ditentukan pada soal kemudian dikirim ke folder yang sudah dibuat sebelumnya, disimpan sebagai file `result.txt`. 
#### Kendala ####
Tidak ada

### Soal 2.d ###
Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya
request, berapa banyak requests yang menggunakan user-agent curl?
Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang
telah dibuat sebelumnya.
#### Dokumentasi Source Code ####
```bash
function solve_2d {
    #cek user agent
    local total_user_agent_curl=$(cat "log_website_daffainfo.log" | awk '
    BEGIN {FS=":"}
    /curl/ {printf "%s\n", $0}
    ' | wc -l) 
    echo -e "\nAda $total_user_agent_curl requests yang menggunakan curl sebagai user-agent\n" >> "$FORENSIC_LOGDIR/result.txt"
}
```
#### Cara Pengerjaan ####
1. Membuat variabel lokal `total_user_agent_curl` untuk menghitung banyaknya request yang menggunakan user-agent curl
2. Caranya adalah dengan menggunakan `awk`. Pertama, isi dari `log_website_daffainfo.log` akan dikeluarkan kemudian hasilnya akan dikirimkan ke awk. Digunakan field separator `:` sebagai pemisah. Lalu setiap barisnya akan difilter dengan syarat harus ada string `curl`.
3. Kemudian hasil dari `awk` itu akan dikirim dan dihitung jumlah barisnya menggunakan syntax `wc -l`
4. Hasil dari perhitungan tersebut akan dicetak sesuai dengan format yang sudah ditentukan pada soal. Kemudian akan dikirim ke folder yang sudah dibuat sebelumnya, disimpan di file `result.txt`.

#### Kendala ####
Tidak ada

### Soal 2.e ###
Pada jam 2 pagi pada tanggal 23 terdapat serangan pada website, Dapos ingin
mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian
masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat
sebelumnya.
#### Dokumentasi Source Code ####
```bash
function solve_2e {
    #loop through the file
    while read p
    do
        # echo "$p"
        echo "$p" | awk 'BEGIN {FS=":"}
        {if($0 ~ "2022:02" ){print substr($1, 2, length($1)-2)}}
        ' >> "$FORENSIC_LOGDIR/result.txt"
    done < "log_website_daffainfo.log"
}
```
#### Cara Pengerjaan ####
1. Digunakan looping untuk membaca setiap baris. Dimana file `log_website_daffainfo.log` akan digunakan sebagai input dari loop tersebut. 
2. Kemudian setiap baris tersebut ditampilkan dan dikirim ke `awk`
3. Pada `awk`, digunakan field separator `:` untuk membagi setiap baris menjadi beberapa bagian. Kemudian akan difilter dengan menggunakan syarat tanggal harus `2022:02` (jam 2 pagi).
4. Akan diambil substring 1 saja (alamat IP saja, tanpa tanda petik). Lalu hasil `awk` akan dikirim ke folder yang sudah dibuat sebelumnya, disimpan ke file `result.txt`.

#### Kendala ####
Tidak ada

## Nomor 3 ##
Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(.
Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untukmemperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal
sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut.
Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada
laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat
suatu program monitoring resource yang tersedia pada komputer.
Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan
monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan
command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`.
Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path
yang akan dimonitor adalah /home/{user}/.

### Soal 3.a ###
Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log.
{YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan
pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah
metrics_20220131150000.log.

#### Dokumentasi Source Code ####
```bash
function main {
    #prep folder
    mkdir -p "$HOME/log/"
    #prefix
    prefix_metrix="mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
    #get free data
    get_free_data
    #get estimate filespace usage of home/$user
    get_estm_filesp_usg

    #combine it
    info="$extracted_free_data,$HOME/,$home_user_usg"
    # echo "$info"
    #get current date in YmdHmS format (yearmonthdayhourminutesecond)
    cur_date=`date +%Y%m%d%H%M%S`
    
    #save it into log file
    echo "$prefix_metrix" > "$HOME/log/metrics_$cur_date.log"
    echo "$info" >> "$HOME/log/metrics_$cur_date.log"
    # echo "Completed"
    #change permission
    #0 is all off, 1 is execute, 2 is write, 4 is read, any is addition
    #xxx, user-grup-other users
    #file is created using cron and then we chmod it... weit, no this must end first oh shi, hm, maybe sabi
    chmod 400 "$HOME/log/metrics_$cur_date.log"
    #this then will be saved and chmoded fuuuuk, then kemana dia tau filenya
    # echo -e "$prefix_metrix\n$info"
}
```
```bash
function get_free_data {
    #save output to txt, so that we coud read it one by one (line)
    free -m > "free.txt"

    #awk it, but first read it
    local counter=0
    local mem_info=""
    local swap_info=""
    # local
    while read p
    do
        if [ $counter -gt 0 ]
        then
            if [ $counter -eq 1 ]
            then
                #mem info
                mem_info=$(echo "$p" | awk '
                {printf "%s,%s,%s,%s,%s,%s,", $2, $3, $4, $5, $6, $7}
                ')
                # echo "$mem_info"
            else
                #swap info
                swap_info=$(echo "$p" | awk '
                {printf "%s,%s,%s", $2, $3, $4}
                ')
                # echo "$swap_info"
            fi
        fi
        counter=$(($counter + 1))
    done < "free.txt"
    #save it to global var
    extracted_free_data="$mem_info$swap_info"
    # echo "$extracted_free_data"
    #remove the unnecessary file
    rm "free.txt"
}
```
```bash
function get_estm_filesp_usg {
    home_user_usg=$(du -sh "$HOME" | awk '
    {print $1}
    ')
}
```
#### Cara Pengerjaan ####
1. Membuat folder terlebih dahulu di log untuk simpan hasil monitoringnya
2. Prefix ada mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
3. Pada fungsi `get_free_data` mengambil data-data 
4. Pada fungsi `get_estm_filesp_usg` mengambil data command `du -sh`
5. Dari data yang telah diambil, kita simpan ke dalam variable  `info` yang nantinya bersama dengan prefix akan kita masukkan ke dalam file log.
6. Mengambil data saat ini dengan format sesuai soal menggunakan command `date +%Y%m%d%H%M%S`
7. Menyimpan hasil yang di dapat ke dalam file log dengan nama sesuai soal yakni "metrics_{date}.log pada directory /home/{user}/log/
8. Mengubah permission file agar hanya dapat dibaca oleh user saat ini, dengan command `chmod 400 "$HOME/log/metrics_$cur_date.log"`

Penjelasan get_free_data:
1. Simpan output dari `free -m` ke dalam temporary text `free.txt`. 
2. Baca isi text tersebut tiap linenya, dan sesuai dengan line tersebut kita gunakan `awk` untuk mengekstrak bagian dari line yang diinginkan yakni kolom ke 2-7 untuk memory, dan 2-4 untuk swap, dan di print ke dalam format sesuai soal. (Dipisah dengan ',')
3. Hasil data yang di filter tersebut di simpan ke dalam variable global `extracted_free_data`

Penjelasan get_estm_filesp_usg:
1. kita extract data dari command `du - sh` dengan menggunakan  `awk` dan mengambil isi kolom pertama yang berisi data ukuran dari path home/{user}.

#### Kendala ####
Tidak ada

### Soal 3.b ###
Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap
menit.

#### Dokumentasi Source Code ####
```bash
SHELL=/bin/bash
`* * * * * bash /home/{user}/{path menuju script}/minute_log.sh >/tmp/debug.txt 2>&1`
```

#### Cara Pengerjaan ####
1. Agar script berjalan otomatis setiap menit, kita buat cron job dengan perintah seperti berikut: 
`* * * * * bash /home/{user}/{path menuju script}/minute_log.sh >/tmp/debug.txt 2>&1`
debug.txt di sini hanya optional untuk mengecek apakah cron berjalan dengan benar.
lalu * * * * * menandakan cron akan berjalan tiap menit

#### Kendala ####
Tidak ada

### Soal 3.c ###
Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script
agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file
agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap
metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis.
Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format
metrics_agg_{YmdH}.log

#### Dokumentasi Source Code ####
Fungsi helper
```bash
#return max between 2 input integer
function get_max {
    if [ $1 -gt $2 ]
    then
        echo "$1"
    else
        echo "$2"
    fi
}
#like max but min
function get_min {
    if [ $1 -lt $2 ]
    then
        echo "$1"
    else
        echo "$2"
    fi
}

function GB_2_MB {
    #me get gb
    local mbed=$(($1 * 1024))
    echo "$mbed"
}

#give the exp math it will calc
function calc_float {
    echo "$1" | bc -l
}


function MB_2_GB {
    #me get mb
    calc_float "$1 / 1024"
}

function process_MB_GB {
    local savedSize=$1
    local unitSize="M"
    local isBigger=$(echo "$savedSize > 1024" | bc )
    if [ $isBigger == 1 ]
    then
        #convert to gb
        savedSize=$(MB_2_GB $savedSize)
        savedSize="$(printf "%.1f" "$savedSize")"
        unitSize="G"
    fi
    local endSizeInfo="$savedSize$unitSize"
    echo "$endSizeInfo"
}
```
```bash
function main {
    #prepare dir for safe
    mkdir -p "$HOME/log/"
    prefix_hour="type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"

    one_hour_ago_date=$(date --date='1 hour ago' "+%Y%m%d%H")

    filename_pf="metrics_$one_hour_ago_date"
    #get all log file that is one hour ago
    prev_h_log_list=$(ls -1 $HOME/log/ | awk -v patt="$filename_pf" '
    {if($0 ~ patt){print $0}}
    ' )

    #convert list of name into array   
    TEMP_IFS=$IFS
    #change ifs to new line
    IFS=$'\n'
    #convert to array sir
    prev_h_log_list=($prev_h_log_list)
    local arr_len=${#prev_h_log_list[@]}
    #preserve past ifs
    IFS=$TEMP_IFS

    #stat file max
    curMaxTotalMem=-1
    curMaxUsedMem=-1
    curMaxFreeMem=-1
    curMaxSharedMem=-1
    curMaxBuffMem=-1
    curMaxAvailableMem=-1
    curMaxTotalSwap=-1
    curMaxUsedSwap=-1
    curMaxFreeSwap=-1
    curMaxPathSizeMB=-1

    #stat file min
    curMinTotalMem=100000000
    curMinUsedMem=100000000
    curMinFreeMem=100000000
    curMinSharedMem=100000000
    curMinBuffMem=100000000
    curMinAvailableMem=100000000
    curMinTotalSwap=100000000
    curMinUsedSwap=100000000
    curMinFreeSwap=100000000
    curMinPathSizeMB=100000000

    #stat file sum
    sumTotalMem=0
    sumUsedMem=0
    sumFreeMem=0
    sumSharedMem=0
    sumBuffMem=0
    sumAvailableMem=0
    sumTotalSwap=0
    sumUsedSwap=0
    sumFreeSwap=0
    sumPathSizeMB=0

    local counter=0
    #open each file, and read each line of the file
    for ((i=0;i<$arr_len;i++))
    do
        counter=0
        curlog_name="${prev_h_log_list[$i]}"
        #read it sir
        while read p
        do
            if [ $counter -eq 1 ]
            then
                #means this is the second line where the number go
                #divide it sir, change it into array
                # echo "$p"
                #convert p into array   
                TEMP_IFS=$IFS
                #change ifs to comma
                IFS=","
                #convert to array sir
                p=($p)
                local total_met=${#p[@]}
                # echo "$total_met"
                #preserve past ifs
                IFS=$TEMP_IFS

                #do statisthic
                #urutan metrix is:
                #total mem 0
                curMaxTotalMem=$(get_max $curMaxTotalMem ${p[0]})
                curMinTotalMem=$(get_min $curMinTotalMem ${p[0]})
                sumTotalMem=$(($sumTotalMem + ${p[0]}))
                # echo "$sumTotalMem"
                #used mem 1
                curMaxUsedMem=$(get_max $curMaxUsedMem ${p[1]})
                curMinUsedMem=$(get_min $curMinUsedMem ${p[1]})
                sumUsedMem=$(($sumUsedMem + ${p[1]}))
                # echo "$curMaxUsedMem -- $curMinUsedMem"
                #free mem 2
                curMaxFreeMem=$(get_max $curMaxFreeMem ${p[2]})
                curMinFreeMem=$(get_min $curMinFreeMem ${p[2]})
                sumFreeMem=$(($sumFreeMem + ${p[2]}))
                #shared mem 3
                curMaxSharedMem=$(get_max $curMaxSharedMem ${p[3]})
                curMinSharedMem=$(get_min $curMinSharedMem ${p[3]})
                sumSharedMem=$(($sumSharedMem + ${p[3]}))                
                #buff mem 4
                curMaxBuffMem=$(get_max $curMaxBuffMem ${p[4]})
                curMinBuffMem=$(get_min $curMinBuffMem ${p[4]})
                sumBuffMem=$(($sumBuffMem + ${p[4]}))  
                #available mem 5
                curMaxAvailableMem=$(get_max $curMaxAvailableMem ${p[5]})
                curMinAvailableMem=$(get_min $curMinAvailableMem ${p[5]})
                sumAvailableMem=$(($sumAvailableMem + ${p[5]}))                 
                #swap total 6
                curMaxTotalSwap=$(get_max $curMaxTotalSwap ${p[6]})
                curMinTotalSwap=$(get_min $curMinTotalSwap ${p[6]})
                sumTotalSwap=$(($sumTotalSwap + ${p[6]}))                
                #swap used 7
                curMaxUsedSwap=$(get_max $curMaxUsedSwap ${p[7]})
                curMinUsedSwap=$(get_min $curMinUsedSwap ${p[7]})
                sumUsedSwap=$(($sumUsedSwap + ${p[7]}))
                #swap free 8
                curMaxFreeSwap=$(get_max $curMaxFreeSwap ${p[8]})
                curMinFreeSwap=$(get_min $curMinFreeSwap ${p[8]})
                sumFreeSwap=$(($sumFreeSwap + ${p[8]}))            
                #path 9
                #path size 10
                #check if it is gb or mb
                t_size="${p[10]}"
                lensz="${#t_size}"
                # echo "$t_size : $lensz"
                numsz="${t_size:0:$lensz-1}"
                mbfied=$numsz
                # echo "$mbfied"
                # echo "$numsz"
                
                gbmb="${t_size:$lensz-1:1}"
                # echo "$gbmb"
                if [ "$gbmb" == "G" ]
                then
                    #giga, conver to mb
                    mbfied=$(GB_2_MB $numsz)
                fi
                curMaxPathSizeMB=$(get_max $curMaxPathSizeMB $mbfied)
                curMinPathSizeMB=$(get_min $curMinPathSizeMB $mbfied)
                sumPathSizeMB=$(($sumPathSizeMB + $mbfied))
            fi
            counter=$(($counter + 1))
        done < "$HOME/log/$curlog_name"
    done

    #if nothing yet
    if [ $arr_len -eq 0 ]
    then
        return
    fi
    #get stat avg
    avgTotalMem=$(calc_float "$sumTotalMem / $arr_len")
    avgTotalMem="$(printf "%.1f" "$avgTotalMem")"

    avgUsedMem=$(calc_float "$sumUsedMem / $arr_len")
    avgUsedMem="$(printf "%.1f" "$avgUsedMem")"
    
    avgFreeMem=$(calc_float "$sumFreeMem / $arr_len")
    avgFreeMem="$(printf "%.1f" "$avgFreeMem")"
    
    avgSharedMem=$(calc_float "$sumSharedMem / $arr_len")
    avgSharedMem="$(printf "%.1f" "$avgSharedMem")"
    
    avgBuffMem=$(calc_float "$sumBuffMem / $arr_len")
    avgBuffMem="$(printf "%.1f" "$avgBuffMem")"
    
    avgAvailableMem=$(calc_float "$sumAvailableMem / $arr_len")
    avgAvailableMem="$(printf "%.1f" "$avgAvailableMem")"
    
    avgTotalSwap=$(calc_float "$sumTotalSwap / $arr_len")
    avgTotalSwap="$(printf "%.1f" "$avgTotalSwap")"
    
    avgUsedSwap=$(calc_float "$sumUsedSwap / $arr_len")
    avgUsedSwap="$(printf "%.1f" "$avgUsedSwap")"
    
    avgFreeSwap=$(calc_float "$sumFreeSwap / $arr_len")
    avgFreeSwap="$(printf "%.1f" "$avgFreeSwap")"
    
    avgPathSizeMB=$(calc_float "$sumPathSizeMB / $arr_len")
    #make it uh logic gb mb
    avgPathSize=$(process_MB_GB $avgPathSizeMB)
    curMinPathSize=$(process_MB_GB $curMinPathSizeMB)
    curMaxPathSize=$(process_MB_GB $curMaxPathSizeMB)
    
    # echo "$prefix_hour"
    minimum="minimum,$curMinTotalMem,$curMinUsedMem,$curMinFreeMem,$curMinSharedMem,$curMinBuffMem,$curMinAvailableMem,$curMinTotalSwap,$curMinUsedSwap,$curMinFreeSwap,$HOME/,$curMinPathSize"
    # echo "$minimum"
    maximum="maxmimum,$curMaxTotalMem,$curMaxUsedMem,$curMaxFreeMem,$curMaxSharedMem,$curMaxBuffMem,$curMaxAvailableMem,$curMaxTotalSwap,$curMaxUsedSwap,$curMaxFreeSwap,$HOME/,$curMaxPathSize"
    # echo "$maximum"
    average="average,$avgTotalMem,$avgUsedMem,$avgFreeMem,$avgSharedMem,$avgBuffMem,$avgAvailableMem,$avgTotalSwap,$avgUsedSwap,$avgFreeSwap,$HOME/,$avgPathSize"
    # echo "$average"

    pf_hour_name="metrics_agg_"
    cur_date=`date +%Y%m%d%H`
    saved_h_logname="$pf_hour_name$cur_date.log"
    #save it into log file
    echo "$prefix_hour" > "$HOME/log/$saved_h_logname"
    echo "$minimum" >> "$HOME/log/$saved_h_logname"
    echo "$maximum" >> "$HOME/log/$saved_h_logname"
    echo "$average" >> "$HOME/log/$saved_h_logname"
    # echo "Completed"
    #change permission
    #0 is all off, 1 is execute, 2 is write, 4 is read, any is addition
    #xxx, user-grup-other users
    chmod 400 "$HOME/log/$saved_h_logname"    
}
```

#### Cara Pengerjaan ####
1. Kita siap kan directory, dengan `mkdir -p "$HOME/log/"`
2. Membuat variable prefix sesuai arahan soal
3. Mendapatkan date pada 1 jam lalu dengan command `date --date='1 hour ago' "+%Y%m%d%H"` karena yang berubah bisa saja tidak hanya jam, tetapi juga hari, bulan, dan tahun.
4. Membuat variable pattern nama file metrics tiap menit yang akan di analisa dengan format sesuai soal yakni "metrics_{date 1 jam lalu}"
5. Kita kumpulkan nama nama file metrics yang sesuai dengan pattern date 1 jam lalu menggunakan `awk`, lalu menyimpannya dalam array `prev_h_log_list` untuk kita baca filenya satu per satu nanti.
6. Membuat variable-variable yang akan menampung nilai statistik seperti maximum, minimum, dan sum dari data data metrics yang telah di ambil.
7. Melakukan looping tiap file metrics lalu mengextract data yang ada di dalam file lalu melakukan operasi yang diperlukan tiap variablenya seperti `get_max` untuk variable penampung maximum, `get_min` untuk variable minimum, dan penambahan untuk variable jenis sum. Extra spesial case untuk ukuran kita ubah sizenya ke dalam unit MB, jadi jika data dalam ukuran GB kita convert ke MB dengan fungsi `GB_2_MB` yakni mengalikan GB dengan 1024.
8. Menghitung average tiap data dengan membagi nilai sum dengan total data yakni panjang array file metrics yakni `arr_len`. Sama seperti no 7, spesial case untuk size, kita hitung rata-rata dalam MB, lalu jika nilai di dapatkan >= 1GB maka kita convert data dari MB ke GB dengan fungsi `process_MB_GB`.
9. Gabungkan semua data yang telah dihitung dan di dapat ke dalam variable `minimum`, `maximum`, dan `average`.
10. Menyimpan hasil tersebut ke dalam file log dengan sanam sesuai soal yakni "metrics_agg_{date}", kita dapat date dengan command `date +%Y%m%d%H`.
11. Mengubah permission
12. Membuat cron job dengan perintah seperti berikut:
`0 * * * * bash /home/{user}/{path menuju script}/aggregate_minutes_to_hourly_log.sh >/tmp/debug2.txt 2>&1`
0 * * * * akan membuat cron job berjalan tiap jam nya dan untuk file debug hanya untuk memastikan cron telah berjalan dengan lancar.

#### Kendala ####
Tidak ada

### Soal 3.d ###
Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user
pemilik file.

#### Dokumentasi Source Code ####
```bash
chmod 400 "$HOME/log/$saved_h_logname"
```

#### Cara Pengerjaan ####
1. Setelah menyimpannya kita ubah permissionnya menjadi read only untuk user dengan comman `chmod 400 "$HOME/log/$saved_h_logname"`. 400 di sini sama seperti untuk script menit, 4 menandakan operasi read, 0 0 menandakan null untuk selain user.

#### Kendala ####
Tidak ada

## Dokumentasi ##
Screenshot main.sh soal 1

![Screenshot terminal main.sh](images/SSmainsh.png)

Screenshot register.sh soal 1

![Screenshot terminal registr.sh](images/ssregistersh.png)
