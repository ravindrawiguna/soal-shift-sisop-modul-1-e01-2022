#return max between 2 input integer
function get_max {
    if [ $1 -gt $2 ]
    then
        echo "$1"
    else
        echo "$2"
    fi
}
#like max but min
function get_min {
    if [ $1 -lt $2 ]
    then
        echo "$1"
    else
        echo "$2"
    fi
}

function GB_2_MB {
    #me get gb
    local mbed=$(($1 * 1024))
    echo "$mbed"
}

#give the exp math it will calc
function calc_float {
    echo "$1" | bc -l
}


function MB_2_GB {
    #me get mb
    calc_float "$1 / 1024"
}

function process_MB_GB {
    local savedSize=$1
    local unitSize="M"
    local isBigger=$(echo "$savedSize > 1024" | bc )
    if [ $isBigger == 1 ]
    then
        #convert to gb
        savedSize=$(MB_2_GB $savedSize)
        savedSize="$(printf "%.1f" "$savedSize")"
        unitSize="G"
    fi
    local endSizeInfo="$savedSize$unitSize"
    echo "$endSizeInfo"
}

function main {
    #prepare dir for safe
    mkdir -p "$HOME/log/"
    prefix_hour="type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"

    one_hour_ago_date=$(date --date='1 hour ago' "+%Y%m%d%H")
    filename_pf="metrics_$one_hour_ago_date"
    #get all log file that is one hour ago
    prev_h_log_list=$(ls -1 $HOME/log/ | awk -v patt="$filename_pf" '
    {if($0 ~ patt){print $0}}
    ' )

    #convert list of name into array   
    TEMP_IFS=$IFS
    #change ifs to new line
    IFS=$'\n'
    #convert to array sir
    prev_h_log_list=($prev_h_log_list)
    local arr_len=${#prev_h_log_list[@]}
    #preserve past ifs
    IFS=$TEMP_IFS

    #stat file max
    curMaxTotalMem=-1
    curMaxUsedMem=-1
    curMaxFreeMem=-1
    curMaxSharedMem=-1
    curMaxBuffMem=-1
    curMaxAvailableMem=-1
    curMaxTotalSwap=-1
    curMaxUsedSwap=-1
    curMaxFreeSwap=-1
    curMaxPathSizeMB=-1

    #stat file min
    curMinTotalMem=100000000
    curMinUsedMem=100000000
    curMinFreeMem=100000000
    curMinSharedMem=100000000
    curMinBuffMem=100000000
    curMinAvailableMem=100000000
    curMinTotalSwap=100000000
    curMinUsedSwap=100000000
    curMinFreeSwap=100000000
    curMinPathSizeMB=100000000

    #stat file sum
    sumTotalMem=0
    sumUsedMem=0
    sumFreeMem=0
    sumSharedMem=0
    sumBuffMem=0
    sumAvailableMem=0
    sumTotalSwap=0
    sumUsedSwap=0
    sumFreeSwap=0
    sumPathSizeMB=0

    local counter=0
    #open each file, and read each line of the file
    for ((i=0;i<$arr_len;i++))
    do
        counter=0
        curlog_name="${prev_h_log_list[$i]}"
        #read it sir
        while read p
        do
            if [ $counter -eq 1 ]
            then
                #means this is the second line where the number go
                #divide it sir, change it into array
                # echo "$p"
                #convert p into array   
                TEMP_IFS=$IFS
                #change ifs to comma
                IFS=","
                #convert to array sir
                p=($p)
                local total_met=${#p[@]}
                # echo "$total_met"
                #preserve past ifs
                IFS=$TEMP_IFS

                #do statisthic
                #urutan metrix is:
                #total mem 0
                curMaxTotalMem=$(get_max $curMaxTotalMem ${p[0]})
                curMinTotalMem=$(get_min $curMinTotalMem ${p[0]})
                sumTotalMem=$(($sumTotalMem + ${p[0]}))
                # echo "$sumTotalMem"
                #used mem 1
                curMaxUsedMem=$(get_max $curMaxUsedMem ${p[1]})
                curMinUsedMem=$(get_min $curMinUsedMem ${p[1]})
                sumUsedMem=$(($sumUsedMem + ${p[1]}))
                # echo "$curMaxUsedMem -- $curMinUsedMem"
                #free mem 2
                curMaxFreeMem=$(get_max $curMaxFreeMem ${p[2]})
                curMinFreeMem=$(get_min $curMinFreeMem ${p[2]})
                sumFreeMem=$(($sumFreeMem + ${p[2]}))
                #shared mem 3
                curMaxSharedMem=$(get_max $curMaxSharedMem ${p[3]})
                curMinSharedMem=$(get_min $curMinSharedMem ${p[3]})
                sumSharedMem=$(($sumSharedMem + ${p[3]}))                
                #buff mem 4
                curMaxBuffMem=$(get_max $curMaxBuffMem ${p[4]})
                curMinBuffMem=$(get_min $curMinBuffMem ${p[4]})
                sumBuffMem=$(($sumBuffMem + ${p[4]}))  
                #available mem 5
                curMaxAvailableMem=$(get_max $curMaxAvailableMem ${p[5]})
                curMinAvailableMem=$(get_min $curMinAvailableMem ${p[5]})
                sumAvailableMem=$(($sumAvailableMem + ${p[5]}))                 
                #swap total 6
                curMaxTotalSwap=$(get_max $curMaxTotalSwap ${p[6]})
                curMinTotalSwap=$(get_min $curMinTotalSwap ${p[6]})
                sumTotalSwap=$(($sumTotalSwap + ${p[6]}))                
                #swap used 7
                curMaxUsedSwap=$(get_max $curMaxUsedSwap ${p[7]})
                curMinUsedSwap=$(get_min $curMinUsedSwap ${p[7]})
                sumUsedSwap=$(($sumUsedSwap + ${p[7]}))
                #swap free 8
                curMaxFreeSwap=$(get_max $curMaxFreeSwap ${p[8]})
                curMinFreeSwap=$(get_min $curMinFreeSwap ${p[8]})
                sumFreeSwap=$(($sumFreeSwap + ${p[8]}))            
                #path 9
                #path size 10
                #check if it is gb or mb
                t_size="${p[10]}"
                lensz="${#t_size}"
                # echo "$t_size : $lensz"
                numsz="${t_size:0:$lensz-1}"
                mbfied=$numsz
                # echo "$mbfied"
                # echo "$numsz"
                gbmb="${t_size:$lensz-1:1}"
                # echo "$gbmb"
                if [ "$gbmb" == "G" ]
                then
                    #giga, conver to mb
                    mbfied=$(GB_2_MB $numsz)
                fi
                curMaxPathSizeMB=$(get_max $curMaxPathSizeMB $mbfied)
                curMinPathSizeMB=$(get_min $curMinPathSizeMB $mbfied)
                sumPathSizeMB=$(($sumPathSizeMB + $mbfied))
            fi
            counter=$(($counter + 1))
        done < "$HOME/log/$curlog_name"
    done

    #if nothing yet
    if [ $arr_len -eq 0 ]
    then
        return
    fi
    #get stat avg
    avgTotalMem=$(calc_float "$sumTotalMem / $arr_len")
    avgTotalMem="$(printf "%.1f" "$avgTotalMem")"

    avgUsedMem=$(calc_float "$sumUsedMem / $arr_len")
    avgUsedMem="$(printf "%.1f" "$avgUsedMem")"
    
    avgFreeMem=$(calc_float "$sumFreeMem / $arr_len")
    avgFreeMem="$(printf "%.1f" "$avgFreeMem")"
    
    avgSharedMem=$(calc_float "$sumSharedMem / $arr_len")
    avgSharedMem="$(printf "%.1f" "$avgSharedMem")"
    
    avgBuffMem=$(calc_float "$sumBuffMem / $arr_len")
    avgBuffMem="$(printf "%.1f" "$avgBuffMem")"
    
    avgAvailableMem=$(calc_float "$sumAvailableMem / $arr_len")
    avgAvailableMem="$(printf "%.1f" "$avgAvailableMem")"
    
    avgTotalSwap=$(calc_float "$sumTotalSwap / $arr_len")
    avgTotalSwap="$(printf "%.1f" "$avgTotalSwap")"
    
    avgUsedSwap=$(calc_float "$sumUsedSwap / $arr_len")
    avgUsedSwap="$(printf "%.1f" "$avgUsedSwap")"
    
    avgFreeSwap=$(calc_float "$sumFreeSwap / $arr_len")
    avgFreeSwap="$(printf "%.1f" "$avgFreeSwap")"
    
    avgPathSizeMB=$(calc_float "$sumPathSizeMB / $arr_len")
    #make it uh logic gb mb
    avgPathSize=$(process_MB_GB $avgPathSizeMB)
    curMinPathSize=$(process_MB_GB $curMinPathSizeMB)
    curMaxPathSize=$(process_MB_GB $curMaxPathSizeMB)
    
    # echo "$prefix_hour"
    minimum="minimum,$curMinTotalMem,$curMinUsedMem,$curMinFreeMem,$curMinSharedMem,$curMinBuffMem,$curMinAvailableMem,$curMinTotalSwap,$curMinUsedSwap,$curMinFreeSwap,$HOME/,$curMinPathSize"
    # echo "$minimum"
    maximum="maxmimum,$curMaxTotalMem,$curMaxUsedMem,$curMaxFreeMem,$curMaxSharedMem,$curMaxBuffMem,$curMaxAvailableMem,$curMaxTotalSwap,$curMaxUsedSwap,$curMaxFreeSwap,$HOME/,$curMaxPathSize"
    # echo "$maximum"
    average="average,$avgTotalMem,$avgUsedMem,$avgFreeMem,$avgSharedMem,$avgBuffMem,$avgAvailableMem,$avgTotalSwap,$avgUsedSwap,$avgFreeSwap,$HOME/,$avgPathSize"
    # echo "$average"

    pf_hour_name="metrics_agg_"
    cur_date=`date +%Y%m%d%H`
    saved_h_logname="$pf_hour_name$cur_date.log"
    #save it into log file
    echo "$prefix_hour" > "$HOME/log/$saved_h_logname"
    echo "$minimum" >> "$HOME/log/$saved_h_logname"
    echo "$maximum" >> "$HOME/log/$saved_h_logname"
    echo "$average" >> "$HOME/log/$saved_h_logname"
    # echo "Completed"
    #change permission
    #0 is all off, 1 is execute, 2 is write, 4 is read, any is addition
    #xxx, user-grup-other users
    #file is created using cron and then we chmod it... weit, no this must end first oh shi, hm, maybe sabi
    chmod 400 "$HOME/log/$saved_h_logname"    
}

main