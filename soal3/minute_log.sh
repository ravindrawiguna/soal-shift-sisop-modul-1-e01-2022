#!/bin/bash

function get_free_data {
    #save output to txt, so that we coud read it one by one (line)
    free -m > "free.txt"

    #awk it, but first read it
    local counter=0
    local mem_info=""
    local swap_info=""
    # local
    while read p
    do
        if [ $counter -gt 0 ]
        then
            if [ $counter -eq 1 ]
            then
                #mem info
                mem_info=$(echo "$p" | awk '
                {printf "%s,%s,%s,%s,%s,%s,", $2, $3, $4, $5, $6, $7}
                ')
                # echo "$mem_info"
            else
                #swap info
                swap_info=$(echo "$p" | awk '
                {printf "%s,%s,%s", $2, $3, $4}
                ')
                # echo "$swap_info"
            fi
        fi
        counter=$(($counter + 1))
    done < "free.txt"
    #save it to global var
    extracted_free_data="$mem_info$swap_info"
    # echo "$extracted_free_data"
    #remove the unnecessary file
    rm "free.txt"
}

function get_estm_filesp_usg {
    home_user_usg=$(du -sh "$HOME" | awk '
    {print $1}
    ')
    # echo "$home_user_usg"
}

function main {
    #prep folder
    mkdir -p "$HOME/log/"
    #prefix
    prefix_metrix="mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
    #get free data
    get_free_data
    #get estimate filespace usage of home/$user
    get_estm_filesp_usg

    #combine it
    info="$extracted_free_data,$HOME/,$home_user_usg"
    # echo "$info"
    #get current date in YmdHmS format (yearmonthdayhourminutesecond)
    cur_date=`date +%Y%m%d%H%M%S`
    
    #save it into log file
    echo "$prefix_metrix" > "$HOME/log/metrics_$cur_date.log"
    echo "$info" >> "$HOME/log/metrics_$cur_date.log"
    # echo "Completed"
    #change permission
    #0 is all off, 1 is execute, 2 is write, 4 is read, any is addition
    #xxx, user-grup-other users
    #file is created using cron and then we chmod it... weit, no this must end first oh shi, hm, maybe sabi
    chmod 400 "$HOME/log/metrics_$cur_date.log"
    #this then will be saved and chmoded fuuuuk, then kemana dia tau filenya
    # echo -e "$prefix_metrix\n$info"
}

main