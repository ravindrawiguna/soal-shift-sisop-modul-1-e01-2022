#!/bin/bash

#=============MSG FUNCTION=========

#dont change this
function show_logo {
    echo " ▄▄▄▄▄▄▄▄▄▄▄   ▄▄▄▄▄▄▄▄▄     ▄▄▄▄     
▐░░░░░░░░░░░▌ ▐░░░░░░░░░▌  ▄█░░░░▌    
▐░█▀▀▀▀▀▀▀▀▀ ▐░█░█▀▀▀▀▀█░▌▐░░▌▐░░▌    
▐░▌          ▐░▌▐░▌    ▐░▌ ▀▀ ▐░░▌    
▐░█▄▄▄▄▄▄▄▄▄ ▐░▌ ▐░▌   ▐░▌    ▐░░▌    
▐░░░░░░░░░░░▌▐░▌  ▐░▌  ▐░▌    ▐░░▌    
▐░█▀▀▀▀▀▀▀▀▀ ▐░▌   ▐░▌ ▐░▌    ▐░░▌    
▐░▌          ▐░▌    ▐░▌▐░▌    ▐░░▌    
▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄█░█░▌▄▄▄▄█░░█▄▄▄ 
▐░░░░░░░░░░░▌ ▐░░░░░░░░░▌▐░░░░░░░░░░░▌
 ▀▀▀▀▀▀▀▀▀▀▀   ▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀ 
                                      "
}


function show_intro_msg {
    show_logo
    echo "Welcome to photo search by E01 group :D"
    echo "You are now in the login page."
}

function show_login_msg {
    echo "You are logged in as $uname"
    echo "Here list of all the command you can do:"
    echo "1. dl N [N is the number of picture you want to download]"
    echo "2. att [View login attempt for this user]"
    echo "3. exit"
}

function get_user_cmd {
    read cmd optional
    # echo $cmd "--" $optional 
    if [ "$cmd" == "dl" ]
    then
        download_picture $optional
    elif [ "$cmd" == "att" ]
    then
        show_login_att
    elif [ "$cmd" == "exit" ]
    then
        echo "Exiting Program..."
        isExit=1
    else
        echo "command not recognized"
    fi
}

function prepare_dir {
    mkdir -p "./user/"

    touch "./user/user.txt"
}
#===============================================

#===============LOGIN FUNCTION================
function get_user_name {
    echo "Username: "
    read uname
}

function get_pass {
    echo "Password: "
    read -s passwd
}

function log_in {
    #check if username is exist
    if [[ -z ${user_pass_map["$uname"]} ]]
    then
        #password to that username is nothing
        echo "Username doesn't exist"
    else
        #exist the password
        if [ "$passwd" == "${user_pass_map["$uname"]}" ]
        then
            #wow success
            echo "====================="
            echo "   Login successful  "
            echo "====================="
            isLogedIn=1
            isDoneLoggingIn=1
            message_code=2
            add_log
        else
            #password did not match
            echo "Failed log in"
            message_code=1
            add_log
        fi
    fi
}

function import_user_password {
    #loop through .user/user.txt
    local counter=1
    local curUser=""
    while read p
    do
        if [ $counter  ==  1 ]
        then
            curUser="$p"
        else
            #counter is > 1, mostlikely 2
            counter=0 #reset it
            #save it
            user_pass_map["$curUser"]="$p"
        fi
        counter=`expr $counter + 1`
    done < "./user/user.txt"
}

function get_power_of_ten {
    compare_p_10=0
    #get passed arg
    local temp=$1
    while [ $temp -gt 0 ]
    do
        compare_p_10=$(($compare_p_10 + 1))
        temp=$(($temp / 10))
    done
    # echo "$1 is categorized as: $compare_p_10"
}

#download picture
function download_picture {
    #passed first argument as total image
    local total_image=$1
    
    local date_now=`date +%F`
    local dir_name="${date_now}_$uname"
    local cur_total_zipped=0
    #check if zip file exist
    FILE_ZIP="$dir_name.zip"
    if [ -f "$FILE_ZIP" ]
    then
        #exist, so unzip
        echo "Already exist a zip file with the same name, unziping it"
        unzip -P "$passwd" "$FILE_ZIP"
        #get current file total file
        cur_total_zipped=`zipinfo -h $FILE_ZIP | awk '/number/ {print $9}'`
        cur_total_zipped=$(($cur_total_zipped - 1))
        echo "found: $cur_total_zipped file inside folder in the zip"
         echo "Deleting old zip file"
        rm "$FILE_ZIP"
        echo "Finished"

    fi
    #download the picture
    for ((i=1;i<=$total_image;i++))
    do  
        echo "downloading image: [$i/$total_image]"
        wget "https://loremflickr.com/320/240" -P "$dir_name"
    done

    #change the name, using awk to get the name
    #save current internal field separator (because we will change it to new line)
    TEMP_IFS=$IFS
    list_img=`ls "$dir_name" -1 -c`
    #change ifs to new line
    IFS=$'\n'
    #convert to array sir
    list_img=($list_img)
    arr_len=${#list_img[@]}
    #preserve past ifs
    IFS=$TEMP_IFS

    #get max power of 10 as prefix
    #by dividing len of array by ten, and ceil it (a.k.a +10 then divide)
    local temp=$(($arr_len + 10))
    local prefix=""
    local power_of_ten=0
    #not in func because need 2 global var
    while [ $temp -gt 0 ]
    do
        power_of_ten=$(($power_of_ten + 1))
        temp=$(($temp / 10))
        prefix+="0"
    done
    # echo "prefix: $prefix"
    local len_dirname="${#dir_name}"
    #loop through the array and change the name
    for ((i=0;i<$arr_len;i++))
    do
        echo "change name [$(($i + 1))/$arr_len]"
        local cur_filename="$dir_name/${list_img[i]}"
        # echo "$cur_filename"
        # echo "${cur_filename:0:4}"
        # echo "${cur_filename:0:$((${#dir_name} + 5))}"
        #check if the name is like PIC
        if [ "${cur_filename:0:$(($len_dirname + 5))}" == "$dir_name/PIC_" ]
        then
            #already in PIC mode, so skip
            # echo "Already in PIC Mode skipping"
            #fix formatin
            local pic_num="${cur_filename:$(($len_dirname + 5)):$((${#cur_filename} - $len_dirname - 5))}"
            # echo "$pic_num"
            #if like curlen if pic num is less than uh len of prefix, then we change them too sir
            if [ ${#pic_num} -lt ${#prefix} ]
            then
                #change the name
                mv "$cur_filename" "$dir_name/PIC_${prefix:0:$((${#prefix} - ${#pic_num}))}$pic_num"
            fi
            continue
        fi
        #process prefix number
        get_power_of_ten $(($cur_total_zipped + 1))
        # if [ $compare_p_10 ] use substring sir
        name_prefix="${prefix:0:$(($power_of_ten - $compare_p_10))}"
        mv "$cur_filename" "$dir_name/PIC_${name_prefix}$(($cur_total_zipped + 1))"
        cur_total_zipped=$(($cur_total_zipped + 1))
    done

    #zip it all reccursively with passowrd
    zip -rP $passwd "$FILE_ZIP" "$dir_name"
    #remove the folder
    rm -rf "$dir_name"

}

function show_login_att {
    #use awk
    echo "Total login attempts for this user $uname:"
    #get log with login and current user
    cat "log.txt" | awk -v user=$uname '
    {if( ($0 ~ user) && ($0 ~ "LOGIN") ) {
        print  $0
    }}' | wc -l
    
}

#======== LOG FUNCTION ================
function add_log {
    touch "log.txt"

    local date_now=`date +"%D %T"`
    # echo "$date_now"
    local message_str=""
    if [ $message_code == 1 ]
    then
        message_str="LOGIN: ERROR Failed login attempt on user $uname"
    elif [ $message_code == 2 ]
    then
        message_str="LOGIN: INFO User $uname logged in"
    else
        message_str="Should not happen, something error"
    fi

    echo "$date_now $message_str" >> "log.txt"

}

#====================================

#=============================================

function main {
    prepare_dir
    message_code=0
    declare -A user_pass_map
    import_user_password
    clear
    show_intro_msg

    echo "================="
    echo "      LOGIN      "
    echo "================="
    
    isLogedIn=0
    isDoneLoggingIn=0
    while [ $isDoneLoggingIn == 0 ]
    do
        get_user_name
        get_pass

        log_in
    done
    
    echo "Log in sir"
    #spesify what user want to do
    isExit=0
    while [ $isExit == 0 ]
    do
        show_login_msg
        get_user_cmd
    done

}

main