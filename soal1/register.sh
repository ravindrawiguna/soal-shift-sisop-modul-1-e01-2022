#!/bin/bash

#=============MSG FUNCTION=========

#dont change this
function show_logo {
    echo " ▄▄▄▄▄▄▄▄▄▄▄   ▄▄▄▄▄▄▄▄▄     ▄▄▄▄     
▐░░░░░░░░░░░▌ ▐░░░░░░░░░▌  ▄█░░░░▌    
▐░█▀▀▀▀▀▀▀▀▀ ▐░█░█▀▀▀▀▀█░▌▐░░▌▐░░▌    
▐░▌          ▐░▌▐░▌    ▐░▌ ▀▀ ▐░░▌    
▐░█▄▄▄▄▄▄▄▄▄ ▐░▌ ▐░▌   ▐░▌    ▐░░▌    
▐░░░░░░░░░░░▌▐░▌  ▐░▌  ▐░▌    ▐░░▌    
▐░█▀▀▀▀▀▀▀▀▀ ▐░▌   ▐░▌ ▐░▌    ▐░░▌    
▐░▌          ▐░▌    ▐░▌▐░▌    ▐░░▌    
▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄█░█░▌▄▄▄▄█░░█▄▄▄ 
▐░░░░░░░░░░░▌ ▐░░░░░░░░░▌▐░░░░░░░░░░░▌
 ▀▀▀▀▀▀▀▀▀▀▀   ▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀ 
                                      "
}


function show_intro_msg {
    show_logo
    echo "Welcome to photo search by E01 group :D"
    echo "You are now in the registration page."
}

function get_confirmation {
    isAccepted=0
    echo "[y/n]"
    local answer="n"
    read answer
    # echo "$answer"
    if [[ "$answer" == "y" || "$answer" == "Y" ]];
    then
        isAccepted=1
    fi
}

function prepare_dir {
    mkdir -p "./user/"

    touch "./user/user.txt"
}

#=================================================

#============ REGISTER FUNCTION====================
function get_user_name {
    echo "Username: "
    read uname
}

function validate_pass {
    local hasMoreThanEight=0
    if [[ ${#passwd} -ge 8 ]]
    then
        hasMoreThanEight=1
    else
        echo "Password must be at least 8 characters long"
    fi

    local hasAtleastOneUpper=0
    #per oper oper
    local totalUpper=$(echo "$passwd" | grep -o '[A-Z]' | wc -l)
    if [ $totalUpper -ge 1 ]
    then
        hasAtleastOneUpper=1
    else
        echo "Password must have at least one uppercase letter"
    fi

    local hasAtleastOneLower=0
    local totalLower=$(echo "$passwd" | grep -o '[a-z]' | wc -l)
    if [ $totalLower -ge 1 ]
    then
        hasAtleastOneLower=1
    else
        echo "Password must have at least one lowercase letter"
    fi

    #alpha numeric test
    local isAlphaNumeric=1
    if [[ "$passwd" =~ [^a-zA-Z0-9] ]]
    then
        echo "Password must only contains alphanumeric character"
        isAlphaNumeric=0
    fi

    #check kesamaan with username
    local isDifferentWithUser=1
    if [[ "$passwd" == "$uname" ]]
    then
        isDifferentWithUser=0
        echo "Password must be different then username"
    fi

    isValidPass=0
    #combine it all, >=8, atleast 1upper,1lower, alpha numerik, diff user
    if [[ $hasMoreThanEight == 1 && $hasAtleastOneLower == 1 && $hasAtleastOneUpper == 1 && $isAlphaNumeric == 1 && $isDifferentWithUser == 1 ]]
    then
        isValidPass=1
    fi
}

function get_pass {
    local isDone=1
    while [ $isDone -eq 1 ]
    do
        echo "Enter password for ["$uname"]: "
        read -s passwd
        echo "Confirm the password (type it again):"
        read -s confirm_pass

        validate_pass
        if [[ $isValidPass == 1 ]]
        then
            if [ $passwd == $confirm_pass ]
            then
                echo "Password confirmed!"
                isDone=$(($isDone + 1))
            else
                echo "Password didn't match"
            fi
        else
            echo "Password is not valid"
        fi
    done
}

function create_account {
    #check if username already exist or not
    if [[ -z ${saved_username_map["$uname"]} ]]
    then
        #means null, so not exist, so acc
        echo "$uname" >> "user/user.txt" && echo "Successfully create account"   #add to last line to user, and give sign it is succed
        echo "$passwd" >> "user/user.txt" && echo "Successfully added password"   #add to last line to user, and give sign it is succed
    else
        #oh no already exist
        echo "Username already exist"
        message_code=1
        add_log
    fi
}

function get_saved_user {
    #loop through .user/user.txt
    local counter=1
    while read p
    do
        if [ $counter  ==  1 ]
        then
            # echo "$p"
            #insert it to the map
            saved_username_map["$p"]=1
        else
            #counter is > 1, mostlikely 2
            counter=0 #reset it
        fi
        counter=`expr $counter + 1`
    done < "./user/user.txt"
}

#===============================================

#======== LOG FUNCTION ================
function add_log {
    touch "log.txt"

    local date_now=`date +"%D %T"`
    # echo "$date_now"
    local message_str=""
    if [ $message_code == 1 ]
    then
        message_str="REGISTER: ERROR User already exists"
    elif [ $message_code == 2 ]
    then
        message_str="REGISTER: INFO User $uname registered successfully"
    else
        message_str="Should not happen, something error"
    fi

    echo "$date_now $message_str" >> "log.txt"

}

#====================================

function main {
    message_code=0
    prepare_dir
    #create empty array to hold saved username
    declare -A saved_username_map
    get_saved_user
    
    clear
    show_intro_msg
    
    echo "======================"
    echo "    CREATE ACCOUNT    "
    echo "======================"

    echo "Please enter this data to complete registration"
    get_user_name
    get_pass

    echo "Create this account?"
    echo "username: "$uname
    get_confirmation
    if [ $isAccepted -eq 1 ]
    then
        echo "Creating account ..."
        create_account
        if [ $message_code != 1 ]
        then
            message_code=2
            add_log
        fi
    else
        echo "Abort creating account"
    fi
}

main