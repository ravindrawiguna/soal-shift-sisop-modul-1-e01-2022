function calculate_seconds {
    #$1, $2, $3 is h, m, s
    echo "$(($1 * 3600 + $2 * 60 + $3))"
}

function calc_float {
    echo "$1" | bc -l
}

function solve_2b {
    #get rata-rata request per jam, baru masukin hasilnya ke ratarata.txt di folder atas
    #use awk sir
    #save the time on req_times
    local req_times=$(cat "log_website_daffainfo.log" | awk '
    BEGIN {FS=":"}
    {printf "%s:%s:%s\n", $3, $4, substr($5, 1, 2)}
    ')
    #convert req_times to array    
    TEMP_IFS=$IFS
    #change ifs to new line
    IFS=$'\n'
    #convert to array sir
    req_times=($req_times)
    local arr_len=${#req_times[@]}
    #preserve past ifs
    IFS=$TEMP_IFS

    #the table name
    # echo "${req_times[0]}"
    #start of first time
    local start_time="${req_times[1]}"
    local h_start=${start_time:0:2}
    local m_start=${start_time:3:2}
    local s_start=${start_time:6:2}
    # echo "$h_start:$m_start:$s_start"

    # echo "total second $(calculate_seconds $h_start $m_start $s_start)"
    local seconds_start=$(calculate_seconds $h_start $m_start $s_start)
    # echo "${req_times[1]}"
    #last time
    local end_time="${req_times[$arr_len-1]}"
    local h_end=${end_time:0:2}
    local m_end=${end_time:3:2}
    local s_end=${end_time:6:2}
    # echo "$h_end:$m_end:$s_end"
    # echo "total second $(calculate_seconds $h_end $m_end $s_end)"
    local seconds_end=$(calculate_seconds $h_end $m_end $s_end)
    # echo "${req_times[$arr_len-1]}"
    # echo "$arr_len"

    local total_seconds=$(($seconds_end - $seconds_start))
    # echo "$total_seconds"
    local req_per_sec=$(calc_float "$(($arr_len - 1))/$total_seconds")
    local req_per_hour=$(calc_float "$req_per_sec * 3600")

    # echo $req_per_hour
    # printf "1. Average request per hour is %.2f req/h\n" $req_per_hour
    # printf "Average request per hour is %.2f req/h\n" $req_per_hour > "$FORENSIC_LOGDIR/ratarata.txt"
    printf "Rata-rata serangan adalah sebanyak %.2f requests per jam" $req_per_hour > "$FORENSIC_LOGDIR/ratarata.txt"
}

function solve_2c {
    local ip_list=$(cat "log_website_daffainfo.log" | awk '
    BEGIN {FS=":"}
    {printf "%s\n", $1}
    ')

    #convert it to array    
    TEMP_IFS=$IFS
    #change ifs to new line
    IFS=$'\n'
    #convert to array sir
    ip_list=($ip_list)
    local arr_len=${#ip_list[@]}
    #preserve past ifs
    IFS=$TEMP_IFS
    # echo "$arr_len"
    declare -A ip_map
    #mappuh
    local modusIP=""
    local curMax=-1
    for ((i=0;i<$arr_len;i++))
    do
        # echo "${ip_list[$i]}"
        #check if ip map of this ip is null
        local curIP=${ip_list[$i]}
        if [ -z ${ip_map["$curIP"]} ]
        then
            #tidak ada, so null
            ip_map["$curIP"]=1
            # echo "${ip_list[$i]} is 1"
        else
            #ada sir, increment
            # echo "mantap"
            ip_map["$curIP"]=$((${ip_map["$curIP"]} + 1))
            if [ ${ip_map["$curIP"]} -gt $curMax ]
            then
                # echo "${ip_list[$i]} is ${ip_map["${ip_list[$i]}"]}"
                #update curmax and modusIP
                curMax=${ip_map["$curIP"]}
                modusIP=${ip_list[$i]}
            fi
        fi
    done
    # echo "2. IP with most request is: $modusIP, having $curMax requests" 
    # echo "IP with most request is: $modusIP, having $curMax requests" > "$FORENSIC_LOGDIR/result.txt"
    echo "IP yang paling banyak mengakses server adalah: ${modusIP:1:${#modusIP}-2} sebanyak $curMax" > "$FORENSIC_LOGDIR/result.txt"

}

function solve_2d {
    #cek user agent
    local total_user_agent_curl=$(cat "log_website_daffainfo.log" | awk '
    BEGIN {FS=":"}
    /curl/ {printf "%s\n", $0}
    ' | wc -l) 
    echo -e "\nAda $total_user_agent_curl requests yang menggunakan curl sebagai user-agent\n" >> "$FORENSIC_LOGDIR/result.txt"
}

function solve_2e {
    #loop through the file
    while read p
    do
        # echo "$p"
        echo "$p" | awk 'BEGIN {FS=":"}
        {if($0 ~ "2022:02" ){print substr($1, 2, length($1)-2)}}
        ' >> "$FORENSIC_LOGDIR/result.txt"
    done < "log_website_daffainfo.log"


}

function main {
    #2a
    FORENSIC_LOGDIR="forensic_log_website_daffainfo_log"
    mkdir -p "$FORENSIC_LOGDIR"

    #do 2b
    solve_2b
    #do 2c
    solve_2c
    #do 2d
    solve_2d
    #do 2e
    solve_2e
}

main